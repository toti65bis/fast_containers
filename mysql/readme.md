# Instalar Mysql en kubernates

```bash
kubectl create -f  https://gitlab.com/toti65bis/fast_containers/-/raw/master/mysql/mysql-hostpath.yaml
```

```bash
kubectl create -f  https://gitlab.com/toti65bis/fast_containers/-/raw/master/mysql/mysql-pv.yaml
```

```bash
helm install --name mysql --set mysqlRootPassword=rootpassword,mysqlUser=mysql,mysqlPassword=my-password,mysqlDatabase=mydatabase,persistence.existingClaim=mysql-pvc stable/mysql
```



